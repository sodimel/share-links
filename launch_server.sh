#!/bin/bash

# Edit LOGFILE path if you want, do not forget to create the folder before launching this file.
# Also, you can set your number of workers (the -w 3 below) to [nb of physical cores * 2 + 1] (source: https://docs.gunicorn.org/en/stable/design.html#how-many-workers).
#
# see DEPLOY.md for more information about this file
cd /path/to/your/project/
LOGFILE=/var/log/gunicorn/share-links.log
. .venv/bin/activate
source .env
python3 -m gunicorn website.wsgi -w 3 --bind=localhost:8000 --log-level=info --log-file=$LOGFILE 2>>$LOGFILE
