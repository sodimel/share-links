# Deploy share-links on a server

1. Clone the repo
    ```bash
    git clone git@gitlab.com:sodimel/share-links.git
    cd share-links
    ```
2. Create the venv
    ```bash
    python3 -m venv .venv
    ```
3. Enter the venv
    ```bash
    . .venv/bin/activate
    ```
4. Create a `.env` file containing something like this:
    ```bash
    export DJANGO_SETTINGS_MODULE=website.settings.base
    export SHARE_LINKS_ALLOWED_HOSTS="your.website-url-here.com"
    export SHARE_LINKS_SECRET_KEY="your secret key here"
    ```
    * *Use this command to get a cool secret key directly from django:*
        ```bash
        python3 -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'
        ```
    * *There's more env vars available in [README.md](README.md).*
5. Source it!
    ```bash
    source .env
    ```
6. Install dependencies
    ```bash
    python3 -m pip install -r requirements.txt
    python3 -m pip install -r requirements_extra.txt  # only if you want to use WeasyPrint to automatically generate pdf of webpages!
    ```
7. Launch migrations
    ```bash
    python3 manage.py migrate
    ```
8. Create your superuser account
    ```bash
    python3 manage.py createsuperuser
    ```

You now have your project on your server, nice! Now you just need to launch a program and voilà it's on internet (spoiler; it *may* be more than that).

> *Warning!* Beyond this point, this guide explain you how to launch your server using gunicorn & apache!

1. Install your favorite wsgi in your venv:
    ```bash
    python3 -m pip install gunicorn
    ```
2. Launch your wsgi client on your server:
    ```bash
    python3 -m gunicorn website.wsgi
    ```
3. Configure your server to expose the wsgi server on the web
    1. Activate the `proxy_http` module
        ```bash
        sudo a2enmod proxy_http
        ```
    2. Create your `website.conf` file (and put it in `/etc/apache2/sites-enabled/`):
        ```apacheconf
        <VirtualHost *:80>
            ServerName XXX
            Redirect permanent / https://XXX/
        </VirtualHost>

        <VirtualHost *:443>

            ServerName XXX

            ServerAdmin XXX

            ProxyPreserveHost On
            ProxyPass /static/ !
            ProxyPass /media/ !
            ProxyPass / http://localhost:8000/
            ProxyPassReverse / http://localhost:8000/

            RequestHeader set X-FORWARDED-PROTOCOL ssl
            RequestHeader set X-FORWARDED-SSL on

        </VirtualHost>
        ```
    3. Create something that let you start your server. For example, here is a systemd service that I named `gunicorn.service` and saved in `/lib/systemd/system/`:
        ```toml
        [Unit]
        Description=Start gunicorn daemon, run share links instance.
        After=network.target

        [Service]
        Type=simple
        User=username
        ExecStart=/path/to/your/file/launch_server.sh
        PrivateTmp=true

        [Install]
        WantedBy=multi-user.target
        ```
        * If you used this snippet, you'll need to re-read your systemd config using this command:
            ```bash
            sudo systemctl daemon-reload
            ```
        * And then enable your service in order to let it run after a boot automatically:
            ```bash
            sudo systemctl enable gunicorn.service
            ```
    4. Start your service:
        ```
        sudo systemctl start gunicorn.service
        ```
        *This will execute `/path/to/your/file/launch_server.sh`, which is the file you can find at the root of this repo, and will enter the folder, activate the venv, source the .env file, and launch the server using gunicorn.*

        *See the comments in the file for more config.*

        **You NEED to update `launch_server.sh` because I don't know where you will put your instance and there's a path in the file. Do not forget this!**
4. *You may want to add a DNS entry pointing to your (sub)domain. I did this in my zone file: `links 3600 IN A <MY_IP>`.*
5. That's all folks!
