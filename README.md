# share-links

Small fast django website that allows you to share links (kinda like shaarli but using Django).

> Version 0.5.1

----

## Screenshots

| ![website screenshot](https://gitlab.com/sodimel/share-links/uploads/9881b550aa2671804aaf92d2493d5901/image.png) | ![mobile screenshot](https://gitlab.com/sodimel/share-links/uploads/ede7cf7f5600f12b95401d1a72fe426b/image.png) | ![admin interface with filters & custom columns](https://gitlab.com/sodimel/share-links/uploads/fc96983d9f920a1aa1bf882a63e33c85/image.png) |
| ---      | ---      | ---- |
| view on computer | view on smartphone | admin interface with filters & custom columns |

----

## Deploy in production

Launch your own instance on the internet! *Do not forget to add your instance url in the webring!*

> See **[`DEPLOY.md`](DEPLOY.md)**

## Install & test (local)

Easily install & launch a django test web server on your own computer! *(you'll need `python3.X-dev` and `gettext`)*

1. Install
    ```bash
    git clone https://gitlab.com/sodimel/share-links.git
    cd share-links
    python3 -m venv .venv
    . .venv/bin/activate
    python3 -m pip install -r requirements.txt  # and python3 -m pip install --upgrade -r requirements.txt for updating packages!
    python3 -m pip install -r requirements_extra.txt  # only if you want to use WeasyPrint to automatically generate pdf of webpages!
    python3 -m pip install -r requirements_dev.txt
    python3 -m pre_commit install
    python3 -m pre_commit autoupdate
    echo "export DJANGO_SETTINGS_MODULE=website.settings.dev" >> .env
    source .env
    python3 manage.py migrate
    echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'ad@m.in', 'admin')" | python3 manage.py shell
    python3 manage.py loaddata default_categories_and_tags  # load default categories & tags
    python3 manage.py runserver 0.0.0.0:8000
    ```
2. Visit the website ([http://localhost:8000/](http://localhost:8000/)).

* *(Optional) Reset db, make migrations, create superuser account in one ugly command*
    ```bash
    rm website/db.sqlite3 && python3 manage.py makemigrations share_links && python3 manage.py migrate && echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'ad@m.in', 'admin')" | python3 manage.py shell
    ```

----

## Features

* a nice django website
* links (with online status)!
* multiple accounts!
* categories (of tags)!
* tags!
* custom actions!
    * add multiple tags to multiple links ([more infos here](https://gitlab.com/sodimel/share-links/-/issues/24#note_1714123948))
* fixtures (provide a default list of categories and tags, translated in fr & en)!
* collections of links!
* comments (for links (with simple math captcha))!
* moderate comments through django admin (publish them, edit them, remove them)!
* rss feeds!
* basic search (search in links (link, title, description) & tags (tag, description))!
* filters (for links & tags)!
* about page!
* translated (in french and in english (you can submit your translations after adding a new language))!
* add file (manually or using weasyprint) to your link (*so that even if the site is down or the webarchive is also down, you have the data*) (can be enabled using env vars or settings)
* it's **f a s t** (I haven't tested it with more than ~3k links but it runs fine using sqlite as a db on an old dell optiplex fx160 with 3 gigs of ram & an old intel atom 230)
* favicons before links! (can be disabled)
* lots of config options!

----

## Config your instance with these lovely environment variables!

*If you don't like env vars you can edit the values in `website/settings/base.py`.*

* `SHARE_LINKS_ALLOWED_HOSTS` (default is `None`): Add the domain name used to reach your instance here (for example for `https://links.l3m.in` I just put `links.l3m.in`).

* `SHARE_LINKS_STATIC_ROOT` (default is `static`): The name of the static folder of your website.

* `SHARE_LINKS_STATICFILES_DIR` (default is `static/`): Path that will be added to `BASE_DIR` in order to find the static dir on your instance. So with the default value it will render to `/path/to/your/instance/of/share-links/ + static/`.

* `SHARE_LINKS_SECRET_KEY` (default is `None`): Put your secret here in this env var. **Very important!**
    * *Use this command to get a cool secret key directly from django:*
        ```bash
        python3 -c 'from django.core.management.utils import get_random_secret_key; print(get_random_secret_key())'
        ```

* `SHARE_LINKS_USE_WEASYPRINT` (default is `False`): Use weasyprint to auto-save pdfs of your webpages (kinda like an archive? idk).  
    **Warning!** This is a very memory-intensive process (expect process times like a minute or two for a *small* server (I have a Dell optiplex fx160 and it's really slow)).  
    **Warning 2** Since `weasyprint` and it's dependencies are huge (several MB), it's not included in `requirements.txt`. Be sure to install it using `python3 -m pip install -r requirements_extra.txt` or else it will not work!

* `SHARE_LINKS_DJANGO_ADMIN_URL` (default is `admin`): Change the admin link for your instance. So for `https://links.example.com` the admin url (if unchanged) would be `https://links.example.com/admin/`.

* `SHARE_LINKS_SHOW_WEBARCHIVE_LINK` (default is `True`): Show a link to the archived link on the detail link pages. *This will not auto-save the page on the web archive!*

* `SHARE_LINKS_DISPLAY_FAVICONS` (default is `True`): Show favicons next to links.

* `SHARE_LINKS_FAVICON_SERVICE_URL` (default is `https://icons.duckduckgo.com/ip3/`): Url of the favicons service used to display favicons next to links.

* `SHARE_LINKS_FAVICON_EXTENSION` (default is `.ico`): Extension to add to the favicon once it has been generated.
    * *Warning! The favicon generation is working by adding `SHARE_LINKS_FAVICON_SERVICE_URL`, `domain_name` and `SHARE_LINKS_FAVICON_EXTENSION`, like this:*
        ```
        [https://icons.duckduckgo.com/ip3/] [l3m.in] [.ico]
        https://icons.duckduckgo.com/ip3/l3m.in.ico
        ```
    * *If you choose to use another favicon service (like `https://www.google.com/s2/favicons?domain=${domain}`), be sure to update the `SHARE_LINKS_FAVICON_EXTENSION` accordingly.*
    * Here's a valid config if you choose to use google's service for favicons:
        ```python
        SHARE_LINKS_FAVICON_SERVICE_URL = "https://www.google.com/s2/favicons?domain="
        SHARE_LINKS_FAVICON_EXTENSION = ""
        ```

* `SHARE_LINKS_SEARCH_MIN_LENGTH` (default is `5`): The min length of the text in order to process the query. High value means less results, but less server load.

* `SHARE_LINKS_PAGINATION_SIZE` (default is `10`): The number of objects that will be displayed on each page.

----

## Load Categories & Tags fixture

There are 9 categories and 100 tags. [See the fixture](website/apps/share_links/fixtures/default_categories_and_tags.json).

```bash
python3 manage.py loaddata default_categories_and_tags
```

----

## Add a language on your instance

1. Add `("fr", _("French")),` (or any language) in the `LANGUAGES` list in `website/settings/base.py`.
2. Generate your translations.
    ```bash
    python3 manage.py makemessages -l fr # edit your language code here!
    ```
3. Translate (edit the new *.po files).
4. Compile your translations.
    ```bash
    python3 manage.py compilemessages -l fr # see comment above
    ```
5. *(optional)* Make a PR?
6. That's all folks!

----

## Join the webring!

If you have an instance running somewhere, just fork the project and add yourself in the `webring.json` file, and submit a merge request (you can also submit your instance link/description in an issue and I will add it myself). No porn or extreme political views/hate speech/fake news instances are allowed in this webring.

----

## Export/Import data

Just use the django-admin commands in your terminal:

### Export

```bash
python3 manage.py dumpdata --exclude auth.permission --exclude contenttypes > db.json
```

Or just share_links & share_links_comments :

```bash
python3 manage.py dumpdata share_links share_links_comments > db.json
```

### Import

```bash
python3 manage.py loaddata db.json
```

----

### Bookmarklets!

You can add bookmarklets in order to be able to save the current page in share-links!

Here's how it will render in your tab bar:

![Share links bookmarklet render](https://gitlab.com/sodimel/share-links/uploads/74dfc593d78a690accb845db47a58a49/image.png)

And here's the code to copy/paste in the "URL" input (you can put whatever you want in the "Name" input):

```js
javascript:location.href='https://[your_instance_here]/[your_admin_url_here]/share_links/link/add/?language=en&link='+location.href
```

If your instance have only one language, you can remove `language=en&` from the code. If you want to save your links using another language, change `en` by the code you want (I have `fr` and `en` in my bookmarks).

Heres' the code for my own instance :

```sh
javascript:location.href='https://links.l3m.in/admin/share_links/link/add/?language=en&link='+location.href
```

*If you're allowing javascript on the admin site and you want to autofill the title & language fields automatically, add `&autofill` to your url in your bookmarklet.*

----

### Browser homepage

You can add your own instance's random link of share-links (or any other one) as your homepage & new window (in Firefox, I have no idea of it's name on the other browsers) in order to always get fresh content when you open your browser! I did this months ago, and it's always cool to (re)discover an interesting tool/article/program/blog.

----

### Management commands

- `check_online_status`: Will update the status of each link ("online" or not) by trying to fetch the title of the link. A dead link will be displayed with a lower opacity on the list, will have a special title available when hovering the box, and it's link will be replaced by a webarchive link.
- `add_missing_titles`: Will check all links: if a link has no title, this script will fetch the title of the page and will put it on the title field.
- `add_missing_languages`: Will check all links and will set the lang attribute value in the language field.
- `delete_stale_comments --yes`: Will remove stale comments (attached to non-existing linkgs). See [the doc](https://django-contrib-comments.readthedocs.io/en/latest/management_commands.html#delete-stale-comments) of `django-contrib-comments` for more infos.

----

### How to add other users?

1) Create a user in the django admin.
2) Tick the "is staff" box.
3) Add permissions on "share_links..." applications (feel free to tweak this).
4) That's all folks!

[Here's the whole process on a small video](https://youtu.be/Bvd5c6yjzJ0).

----

## Todolist (in french)

- [x] traduit
- [x] site django
- [x] log historique (ajout/modifs/deletes) [supprimé parce que inutile]
- [x] commentaires
- [x] rss (issu des logs)
- [x] interface admin (ajout/edit/remove liens/tags)
- [x] fetch screenshot website (jsais pas comment faire :/) - utilisation de weasyprint !
- [x] affiche lien webarchive (save dans l'admin, view dans la vue de détail d'un lien)
- [x] différents filtres
    - [x] récent/ancien
    - [x] mis en avant
    - [x] modifs récentes
    - [x] par site (index + toutes les autres urls entrées)
- [x] recherche
    - [x] par nom du site
    - [x] par contenu de la description (optionel)
    - [x] par tag
    - [x] assez rapide (contrairement à l'époque où ça utilisait thefuzz)
- [x] webring des différentes instances (optionnel)
- [x] règles pour entrer dans le webring (pas de nazis lol)
- [x] modèles
    - [x] lien
        - [x] lien
        - [x] titre
        - [x] description
        - [x] FK tags
        - [x] pas de slug, juste l'id pour aller sur le lien correspondant
    - [x] tag
        - [x] tag
        - [x] description
        - [x] slug
- [x] pages
    - [x] liste des liens
    - [x] liste des tags
    - [x] tag tout seul
    - [x] page about me
    - [x] lien tout seul (ajouter lien sur la date)
    - [x] rss historique liens
    - [x] rss historique tags
    - [x] rss liens (ajout seulement)
    - [x] rss tags (ajout seulement)
    - [x] rss liens + tags (ajout seulement)
    - [x] page de contact
- [ ] messages (signaux)
    - [x] about page unique (si on essaye d'en créer une seconde)
        - [ ] afficher quand même messages si on crée/détruit un objet About (actuellement pas le cas)
    - [ ] autres messages ?
- [ ] SEO des templates
- [x] export des données (csv ? json ? les deux ?)
- [x] lien aléatoire
- [x] stats
- [x] NOUVELLE IDÉE : POUVOIR FAIRE DES LISTES DE LIENS !!!!
    - j'ai envie de pouvoir faire des listes des artistes que j'aime bien, des youtubers que je trouve intéressants (on trouve très peu de listes de youtubers sur le net, c'est un processus long et compliqué d'en trouver des nouveaux de manière "organique", sans formules compliquées d'ia qui nous redirigent vers des youtubers complotistes :P). Cet outil est parfait, puisqu'il permet de stocker des liens. Maintenant il faut trouver la forme adéquate. Edit: trouvé, on a des collections maintenant.
- [x] ajouter config pour weasyprint
- [x] doc déployer son instance
- [x] modération commentaires
- [x] Régler souci menu mobile
- [x] Régler souci largeur de la page sur mobile (overflow ?)
- [x] Régler souci affichage description des tags en entier sur les pages des tags
- [x] Se renseigner pour récupérer le title de la page (et la meta description aussi ?) qu'on entre
- [x] Régler le souci de la page de recherche (+1000 liens = crash ?)
- [x] Créer une page 404 convenable
- [x] ajouter des favicons devant les liens
- [x] Créer page "derniers commentaires".
- [x] Ajouter commentaires dans les stats.
- [x] Check des liens morts & remplacement par lien web archive.
- [x] Récupérer la langue du contenu de la page.
- [x] Afficher nb de liens par langue dans les stats.
- [ ] Rajouter des filtres pour afficher du contenu par langue uniquement (idem pour page random).
- [ ] Ajouter liste des collections auxquels appartient un lien.
- [ ] Ajouter le nombre de collections dans les stats.


## Memo (just for me)

* Commits titles: MAINT, FEAT, FIX, DOCS.
* `python3 -m bumpversion {major, minor, patch}` -> following [semver](https://semver.org/).
* `git push && git push --tag` -> push all changes (even new tags!)
* Do not forget translations!
  * `python3 manage.py makemessages -l fr`
  * `python3 manage.py compilemessages -l fr`
* Launch djhtml from command line:
    ```bash
    . .venv/bin/activate
    python3 -m pip install djhtml  # one time only
    find website -name "*.html" | xargs python3 -m djhtml -i -t 2
    ```
