from pathlib import Path

from .base import *  # noqa

BASE_DIR = Path(__file__).resolve().parent.parent

SECRET_KEY = "django-insecure-!*652volee^im^%_&2yl6qe$-1*esn3*as(cuixna8q^g#f@ng"

DEBUG = True

INTERNAL_IPS = ALLOWED_HOSTS = ["127.0.0.1", "localhost"]


STATIC_URL = "/static/"

SECURE_SSL_REDIRECT = False

MIDDLEWARE = [
    "debug_toolbar.middleware.DebugToolbarMiddleware",
] + MIDDLEWARE  # noqa: F405

INSTALLED_APPS = INSTALLED_APPS + [  # noqa: F405
    "debug_toolbar",
]
