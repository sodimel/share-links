from django.apps import AppConfig


class ShareLinksConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "website.apps.share_links"
