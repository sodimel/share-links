def get_model():
    from website.apps.share_links_comments.models import CommentwithCaptcha

    return CommentwithCaptcha


def get_form():
    from website.apps.share_links_comments.forms import CommentwithCaptchaForm

    return CommentwithCaptchaForm
