��          �      �       H     I  j   U  
   �     �  
   �     �     �       W   
  D   b     �  
   �     �  A  �       u   )     �     �  
   �     �  "   �       ]   	  H   g      �     �     �                                             
         	           Comment URL Comments must be written using <a href="https://daringfireball.net/projects/markdown/basics">Markdown</a>. Link infos Link url Link/Title Moderate comments Nb of link comments Post Supported tags are: a, p, i, b, img, ul, ol, li, code, pre, em, strong, blockquote, br. Thank you for your comment. It will be moderated and published soon. This comment was moderated. User infos admin Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 URL du commentaire Les commentaires peuvent être écrits en <a href="https://daringfireball.net/projects/markdown/basics">Markdown</a>. Infos du lien URL du lien Lien/Titre Modération des commentaires Nombre de commentaires sur le lien Post Les tags supportés sont: a, p, i, b, img, ul, ol, li, code, pre, em, strong, blockquote, br. Merci pour votre commentaire. Il va être modéré et publié d'ici peu. Ce commentaire a été modéré. Infos de l'utilisateur admin 