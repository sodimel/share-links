from django.contrib.admin.apps import AdminConfig


class ShareLinksAdminConfig(AdminConfig):
    default_site = "website.apps.share_links_admin.admin.ShareLinksAdmin"
